namespace Task3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Task3 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CityName = c.String(),
                        Xcoord = c.String(),
                        Ycoord = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CityWeathers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Xcoord = c.String(),
                        YCoord = c.String(),
                        Temp = c.Int(nullable: false),
                        Humidity = c.Int(nullable: false),
                        Windspeed = c.Int(nullable: false),
                        Cty = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Weathers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Xcoord = c.String(),
                        YCoord = c.String(),
                        Temp = c.Int(nullable: false),
                        Humidity = c.Int(nullable: false),
                        Windspeed = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Weathers");
            DropTable("dbo.CityWeathers");
            DropTable("dbo.Cities");
        }
    }
}
