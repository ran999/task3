namespace Task3.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Task3New : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CityWeathers", "Date", c => c.DateTime(nullable: false));
            AddColumn("dbo.Weathers", "Date", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Weathers", "Date");
            DropColumn("dbo.CityWeathers", "Date");
        }
    }
}
