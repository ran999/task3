﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3.Models
{
    public class CityWeather
    {
        public int Id { get; set; }
        public string Xcoord { get; set; }
        public string YCoord { get; set; }
        public int Temp { get; set; }
        public int Humidity { get; set; }
        public int Windspeed { get; set; }
        public string Cty { get; set; }
        public DateTime Date { get; set; }
    }
}
