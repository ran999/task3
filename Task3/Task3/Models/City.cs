﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3.Models
{
    public class City
    {
        public int Id { get; set; }
        public string CityName { get; set; }
        public string Xcoord { get; set; }
        public string Ycoord { get; set; }
    }
}
