﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Task3.Models
{
    public class YandexApiModel
    {
        public int now { get; set; }
        public string now_dt { get; set; }
        public Object info { get; set; }
        public fact fact { get; set; }
        public List<Object> forecasts { get; set; }
    }
    public class fact
    {
        public int temp { get; set; }
        public int feels_like { get; set; }
        public string icon { get; set; }
        public string condition { get; set; }
        public double wind_speed { get; set; }
        public double wind_gust { get; set; }
        public string wind_dir { get; set; }
        public int pressure_mm { get; set; }
        public int pressure_pa { get; set; }
        public int humidity { get; set; }
        public string daytime { get; set; }
        public bool polar { get; set; }
        public string season { get; set; }
        public int prec_type { get; set; }
        public double prec_strength { get; set; }
        public double cloudness { get; set; }
        public long obs_time { get; set; }
    }
}