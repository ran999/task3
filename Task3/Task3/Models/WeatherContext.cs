﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task3.Models
{
    public class WeatherContext:DbContext
    {
        public WeatherContext() : base("DefaultConnection")
        {

        }
        public DbSet<Weather> Weathers { get; set; }
        public DbSet<CityWeather> CityWeathers { get; set; }
        public DbSet<City> Citys { get; set; }
    }
}
