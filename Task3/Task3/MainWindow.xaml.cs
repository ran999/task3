﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Data.Entity;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Task3.Models;
using System.Threading;
using System.Windows.Threading;

namespace Task3
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public DispatcherTimer t = null;
       
        WeatherContext db;
        public MainWindow()
        {
            InitializeComponent();

            db = new WeatherContext();
            db.Citys.Load();
            var citylist = db.Citys.Local.ToBindingList();
            foreach (var item in citylist)
            {
                cityComboBox.Items.Add(item.CityName);
            }
            db.Dispose();
        }
        public string apikey = "da623ccc-0fd5-4037-bcc9-9796768750e8";
        private void checkButton_Click(object sender, RoutedEventArgs e)
        {
            string str = "";
            string x = xcoordBox.Text;
            string y = ycoordBox.Text;
            string site = "https://api.weather.yandex.ru/v1/forecast?lat="+x+"&lon="+y+ "&extra=true";
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(site);
            req.Headers.Add("X-Yandex-API-Key: "+apikey);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            using (StreamReader stream = new StreamReader(
                 resp.GetResponseStream(), Encoding.UTF8))
            {
                str = stream.ReadToEnd();
            }
            YandexApiModel api = JsonConvert.DeserializeObject<YandexApiModel>(str);
            temperature.Text = api.fact.temp.ToString() + " C";
            humidity.Text = api.fact.humidity.ToString() + "%";
            windspeed.Text = api.fact.wind_speed.ToString() + "м/с";
            db = new WeatherContext();
            Weather weath = new Weather();
            weath.Temp = api.fact.temp;
            weath.Humidity = api.fact.humidity;
            weath.Windspeed = Convert.ToInt32(api.fact.wind_speed);
            weath.Xcoord = x;
            weath.YCoord = y;
            weath.Date = DateTime.Now;
            db.Weathers.Add(weath);
            db.SaveChanges();
            db.Dispose();
        }

        private void cityButton_Click(object sender, RoutedEventArgs e)
        {
            db = new WeatherContext();
            City city = db.Citys.Where(c => c.CityName == cityComboBox.SelectedItem.ToString()).FirstOrDefault();
            string str = "";
            string x = city.Xcoord;
            string y = city.Ycoord;
            string site = "https://api.weather.yandex.ru/v1/forecast?lat=" + x + "&lon=" + y + "&extra=true";
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(site);
            req.Headers.Add("X-Yandex-API-Key: " + apikey);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            using (StreamReader stream = new StreamReader(
                 resp.GetResponseStream(), Encoding.UTF8))
            {
                str = stream.ReadToEnd();
            }
            YandexApiModel api = JsonConvert.DeserializeObject<YandexApiModel>(str);
            temperaturecity.Text = api.fact.temp.ToString() + " C";
            humiditycity.Text = api.fact.humidity.ToString() + "%";
            windspeedcity.Text = api.fact.wind_speed.ToString() + "м/с";
            
            CityWeather weath = new CityWeather();
            weath.Temp = api.fact.temp;
            weath.Humidity = api.fact.humidity;
            weath.Windspeed = Convert.ToInt32(api.fact.wind_speed);
            weath.Xcoord = x;
            weath.YCoord = y;
            weath.Cty = city.CityName;
            weath.Date = DateTime.Now;
            db.CityWeathers.Add(weath);
            db.SaveChanges();
            db.Dispose();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            string str = "";
            string x = xcoordBox.Text;
            string y = ycoordBox.Text;
            string site = "https://api.weather.yandex.ru/v1/forecast?lat=" + x + "&lon=" + y + "&extra=true";
            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(site);
            req.Headers.Add("X-Yandex-API-Key: " + apikey);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            using (StreamReader stream = new StreamReader(
                 resp.GetResponseStream(), Encoding.UTF8))
            {
                str = stream.ReadToEnd();
            }
            YandexApiModel api = JsonConvert.DeserializeObject<YandexApiModel>(str);
            temperature.Text = api.fact.temp.ToString() + " C";
            humidity.Text = api.fact.humidity.ToString() + "%";
            windspeed.Text = api.fact.wind_speed.ToString() + "м/с";
            db = new WeatherContext();
            Weather weath = new Weather();
            weath.Temp = api.fact.temp;
            weath.Humidity = api.fact.humidity;
            weath.Windspeed = Convert.ToInt32(api.fact.wind_speed);
            weath.Xcoord = x;
            weath.YCoord = y;
            weath.Date = DateTime.Now;
            db.Weathers.Add(weath);
            
            City city = db.Citys.Where(c => c.CityName == cityComboBox.SelectedItem.ToString()).FirstOrDefault();
            str = "";
            x = city.Xcoord;
            y = city.Ycoord;
            site = "https://api.weather.yandex.ru/v1/forecast?lat=" + x + "&lon=" + y + "&extra=true";
            HttpWebRequest reqcity = (HttpWebRequest)HttpWebRequest.Create(site);
            reqcity.Headers.Add("X-Yandex-API-Key: " + apikey);
            HttpWebResponse respcity = (HttpWebResponse)reqcity.GetResponse();

            using (StreamReader stream = new StreamReader(
                 respcity.GetResponseStream(), Encoding.UTF8))
            {
                str = stream.ReadToEnd();
            }
            YandexApiModel apicity = JsonConvert.DeserializeObject<YandexApiModel>(str);
            temperaturecity.Text = apicity.fact.temp.ToString() + " C";
            humiditycity.Text = apicity.fact.humidity.ToString() + "%";
            windspeedcity.Text = apicity.fact.wind_speed.ToString() + "м/с";

            CityWeather weathcity = new CityWeather();
            weathcity.Temp = apicity.fact.temp;
            weathcity.Humidity = apicity.fact.humidity;
            weathcity.Windspeed = Convert.ToInt32(apicity.fact.wind_speed);
            weathcity.Xcoord = x;
            weathcity.YCoord = y;
            weathcity.Cty = city.CityName;
            weathcity.Date = DateTime.Now;
            db.CityWeathers.Add(weathcity);
            db.SaveChanges();
            db.Dispose();
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            if (timeBox.Text != "")
            {
                t = new DispatcherTimer();
                t.Tick += new EventHandler(timer_Tick);
                t.Interval = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(timeBox.Text) * 1000);
                t.Start();
            }
            else
            {
                MessageBox.Show("Задайте интервал обновления");
            }
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            t.Stop();
        }
    }
}
